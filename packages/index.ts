// export * from './Button/index.js'
// export * from './Icon/index.js'
// export * from './Header/index.js'
// export * from './Page/index.js'

import LButton from './Button/index'

export { LButton }


const componentList = [LButton]
const install = function (Vue) {
  componentList.forEach(component => {
    Vue.component(component.name, component)
  })
}
export default {
  install
}
