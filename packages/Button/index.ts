// packages/button/index.js
import LButton from './src/Button.vue'
LButton.install = function (Vue) {
  Vue.component(LButton.name, LButton)
}

export default LButton;