# rtsim-cli

## 安装依赖

```sh
npm install
```

### 开发

npm run dev:系统名称 没有默认启动全部系统

```sh
npm run dev
```

```sh
npm run dev:deduce
```

### 打包

npm run build:系统名称 没有默认打包全部系统

```sh
npm run build
```

```sh
npm run build:deduce
```

### 预览

```sh
npm run preview
```

### 代码规范自测 Lint style-lint

```sh
npm run lint
npm run style-lint
```

```sh
// 检测+修复
npm run lint:fix
npm run style-lint:fix
```

### 统一格式

```sh
npm run format
```

### git 提交流程

```sh
npm run lint:fix
git add .
npm run commit   // 按流程填写对应提交内容
git pull
git push
```

### rtsim-cli 目录结构

├─.commitlintrc.js              // git 提交规范 
├─.cz-config.js                 // git 自定义提交规范内容
├─.eslintignore                 // eslint忽略文件
├─.eslintrc-auto-import.json    // eslint自动导入配置
├─.eslintrc.js                  // eslint配置
├─.gitignore                    // git忽略文件
├─.npmrc                        // npm配置
├─.prettierignore               // eslint忽略文件
├─.prettierrc.js                // git配置
├─.stylelintignore              // stylelint忽略文件
├─.stylelintrc.js               // stylelint配置
├─auto-imports.d.ts             // 自动导入配置
├─index.html                    // html模板
├─package-lock.json             // package-lock
├─package.json                  // 项目依赖文件
├─tsconfig.json                 // ts配置
├─tsconfig.node.json            // ts node 配置
├─vite.config.ts                // vite 配置
├─viteResolve.config.ts         // vite Resolve项 配置
├─public                        // 项目静态资源
| ├─icons                       // 字体图标
| | └iconfont.js
├─node_modules                  // 项目依赖包
├─envs                          // 项目环境配置
| ├─.env.development
| └.env.production
├─.vscode                       // vscode配置
├─.husky                        // git规范配置
| ├─commit-msg                  
| ├─pre-commit
├─.git                          // 项目git
├─src                           // 项目文件
| ├─App.vue                     // 项目入口
| ├─main.ts                     // 项目入口文件
| ├─vite-env.d.ts
| ├─utils                       // 项目工具函数
| | ├─index.ts                  // 工具函数统一出口
| | ├─tokenUtil.ts              // 项目操作token的类
| | ├─request                   // 项目操作数据交互方法
| | | ├─index.ts                // axios配置
| | | └types.ts
| ├─style                       // 项目样式
| | ├─common.less               // 项目公共样式
| | ├─var.less                  // 项目公共样式变量
| | ├─element                   // element样式
| | | └index.less               
| ├─stores                      // 项目公共状态管理
| | └counter.ts
| ├─router                      // 项目路由
| | ├─index.ts
| | └routes.ts
| ├─layout                      // 项目布局
| | └layoutHome.vue
| ├─data                        // 项目公共数据 枚举 常量
| | └index.ts
| ├─components                  // 项目组件
| | ├─index.ts
| | ├─SvgIcon                   // svg图标组件
| | | └SvgIcon.vue
| ├─commonHook                  // 项目公共hook
| | └usePostRequest.ts
| ├─api                         // 项目接口
| | ├─task
| | | ├─index.ts
| | | └types.ts
| | ├─instance                  // axios实例
| | | ├─createAxiosInstance.ts
| | | └systemInstance.ts
| ├─systems                     // 项目各系统
| | ├─project1                  // 系统1
| | | ├─utils                   // 系统工具函数
| | | | └index.ts
| | | ├─store                   // 系统公共状态管理
| | | | └project.store.ts
| | | ├─routes                  // 系统路由
| | | | └project.route.ts       // 系统路由以.router.ts结尾
| | | ├─pages                   // 系统页面
| | | | ├─projectList
| | | | | └projectList.vue
| | | | ├─projectAdd
| | | | | └projectAdd.vue
| | | ├─data                    // 系统公共数据 枚举 常量
| | | | └index.ts
| | | ├─api                     // 系统接口
| | | | └index.ts
| | | ├─.git                    // 系统git仓库
