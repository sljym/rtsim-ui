import { createApp } from 'vue'
import router from './router/index'
import App from './App.vue'
import '../dist/esm/theme/index.css'
import LUi from '../dist/esm/index.js';

const app = createApp(App)
app.use(router)
app.use(LUi)
app.mount('#app')
