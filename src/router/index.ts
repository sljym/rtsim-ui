import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      name: 'layout',
      path: '/',
      component: () => import('../pages/demo/index.vue')
    }
  ]
})

export default router
