import { resolve } from 'path'

export const viteResolveConfig = {
  // 别名配置
  alias: {
    '@': resolve(__dirname, 'src'),
    '@api': resolve(__dirname, 'src/api'),
    '@utils': resolve(__dirname, 'src/utils'),
    '@systems': resolve(__dirname, 'src/systems'),
    '@components': resolve(__dirname, 'src/components'),
    '@experiment': resolve(__dirname, 'src/systems/experimentDesign'),
    '@deduce': resolve(__dirname, 'src/systems/scenarioDeduce')
  },
  //配置文件扩展名
  extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json']
}
