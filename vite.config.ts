import { resolve } from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import viteCompression from 'vite-plugin-compression'
import AutoImport from 'unplugin-auto-import/vite'
import { createHtmlPlugin } from 'vite-plugin-html'
import { viteResolveConfig } from './viteResolve.config'
import { viteExternalsPlugin } from 'vite-plugin-externals'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'

export default defineConfig({
  base: './', //打包路径

  // 运行 代理 和 打包 配置
  server: {
    host: '0.0.0.0',
    port: 3000,
    open: false,
    https: false,
    proxy: {}
  },

  // 生产环境去除 console debugger
  build: {
    terserOptions: {
      compress: {
        drop_console: true,
        drop_debugger: true
      }
    }
  },

  plugins: [
    vue(),

    // jsx
    // vueJsx(),

    //不打包静态库
    viteExternalsPlugin({
      cesium: 'Cesium'
    }),

 

    // gzip压缩 生产环境生成 .gz 文件
    viteCompression({
      verbose: true,
      disable: false,
      threshold: 10240,
      algorithm: 'gzip',
      ext: '.gz'
    }),

    // 动态创建html模板
    createHtmlPlugin({
      minify: true,
      /**
       * 在这里写entry后，你将不需要在`index.html`内添加 script 标签，原有标签需要删除
       * @default src/main.ts
       */
      entry: '/src/main.ts',
      /**
       * 如果你想将 `index.html`存放在指定文件夹，可以修改它，否则不需要配置
       * @default index.html
       */
      template: '/index.html',
      /**
       * 需要注入 index.html ejs 模版的数据
       */
      inject: {
        data: {
          title: '测试名',
          /**
           * `
                        <link rel="stylesheet" href="//unpkg.com/element-plus/dist/index.css" />
                        <script src="//cdn.jsdelivr.net/npm/vue@3"></script>
                        <script src="//unpkg.com/element-plus"></script>
                        `
           */
          injectScript: ` <script src="/icons/iconfont.js"></script>`
        }
      }
    }),

    createSvgIconsPlugin({
      // 指定需要缓存的图标文件夹
      iconDirs: [resolve(process.cwd(), '/icons')],
      // 指定symbolId格式
      symbolId: 'icon-[dir]-[name]'

      /**
       * 自定义插入位置
       * @default: body-last
       */
      // inject?: 'body-last' | 'body-first'

      /**
       * custom dom id
       * @default: __svg__icons__dom__
       */
      // customDomId: '__svg__icons__dom__',
    })
  ],

  resolve: viteResolveConfig,

  // css: {
  //   preprocessorOptions: {
  //     less: {
  //       additionalData: '@import "@/style/var.less";'
  //     }
  //     // 全局scss文件挂载
  //     // scss: {
  //     //   // additionalData: '@import "@/assets/style/main.less";',
  //     // },
  //   }
  // },
  // envDir: '/envs',

  esbuild: {
    pure: ['console.log', 'debugger']
  }
})
