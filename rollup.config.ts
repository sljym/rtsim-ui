/**
 * @Author: 任强
 * @Date: 2023/3/6 14:32
 * @Version: 1.0
 * @Content:
 */
import { defineConfig } from 'rollup'
import path from 'path'
import nodeResolve from '@rollup/plugin-node-resolve'
import json from '@rollup/plugin-json'
import esbuild from 'rollup-plugin-esbuild'
import less from 'rollup-plugin-less'
import vuePlugin from 'rollup-plugin-vue'

export default defineConfig([
  {
    input: 'packages/index.ts',

    output: {
      dir: path.resolve(__dirname, 'dist/esm'),
      format: 'esm'
    },
    plugins: [
      vuePlugin(),
      esbuild({
        target: 'es2018'
      }),
      nodeResolve(),
      json(),
      less({
        output: path.resolve(__dirname, 'dist/esm/theme/index.css')
      })
    ],
    external: ['vue']
  }
])
